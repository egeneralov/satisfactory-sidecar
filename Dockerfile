FROM golang:1.18.3
WORKDIR /go/src/gitlab.com/egeneralov/satisfactory-sidecar/
ADD go.mod go.sum /go/src/gitlab.com/egeneralov/satisfactory-sidecar/
ARG GOPROXY
RUN go mod download -x
ADD . .
RUN go build -v -ldflags "-v -linkmode auto -extldflags \"-static\"" -o /go/bin/satisfactory-sidecar gitlab.com/egeneralov/satisfactory-sidecar

FROM debian:bullseye
RUN apt-get update -q && apt-get install -yq ca-certificates --no-install-recommends
ENV PATH='/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
CMD /go/bin/satisfactory-sidecar
COPY --from=0 /go/bin /go/bin
