package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	logAdded     = `.*LogNet: AddClientConnection: Added client connection: \[UNetConnection\] RemoteAddr: ((?:[0-9]{1,3}\.){3}[0-9]{1,3}:\d+), Name: ([a-zA-Z]+_\d+).*`
	logRemoved   = `.*LogNet: UChannel::Close: Sending CloseBunch. ChIndex == 0. Name: \[UChannel\] ChIndex: 0, Closing: 0 \[UNetConnection\] RemoteAddr: ((?:[0-9]{1,3}\.){3}[0-9]{1,3}:\d+), Name: ([a-zA-Z]+_\d+).*`
	logState     = ` Update state \(0x\d+\) ([a-z| ]+), progress: (\d+\.\d+) .*`
	logInstalled = `Success! App '\d+' fully installed\.`
)

var (
	reAdded     = regexp.MustCompile(logAdded)
	reRemoved   = regexp.MustCompile(logRemoved)
	reState     = regexp.MustCompile(logState)
	reInstalled = regexp.MustCompile(logInstalled)
)

func NewSideCar() *SideCar {
	return &SideCar{
		Collector: NewCollector(),
	}
}

type SideCar struct {
	Collector *Metrics
}

func (s *SideCar) Print(t time.Duration) {
	for {
		time.Sleep(t)
		if len(s.Collector.StateString) > 1 {
			fmt.Println(s.Collector.StateString)
		} else {
			fmt.Println("Current Players:", s.Collector.Count)
		}
	}
}

func (s *SideCar) Tail(path string) {
	for {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			fmt.Printf("File %s does not exist, waiting 1 second\n", path)
			time.Sleep(time.Second)
		} else {
			fmt.Printf("File %s present, starting\n", path)
			break
		}
	}

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = file.Close()
	}()

	// seek to end of file
	_, err = file.Seek(0, 2)
	if err != nil {
		panic(err)
	}

	// start reading from end of file
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				time.Sleep(10 * time.Millisecond)
				continue
			}
			panic(err)
		}
		switch {
		// if state lines in logs
		case reState.MatchString(line):
			matches := reState.FindStringSubmatch(line)
			if len(matches) == 3 {
				state := matches[1]
				progress, err := strconv.ParseFloat(matches[2], 64)
				if err != nil {
					continue
				}
				s.Collector.State = state
				s.Collector.Progress = progress
			}
			s.Collector.StateString = fmt.Sprintf("State: %s, Progress: %f", s.Collector.State, s.Collector.Progress)
		case reInstalled.MatchString(line):
			s.Collector.State = "installed"
			s.Collector.Progress = 100
			s.Collector.StateString = ""
		// if player added to server
		case reAdded.MatchString(line):
			match := reAdded.FindStringSubmatch(line)
			s.Collector.Count++
			fmt.Printf("Added: %s %s\n", match[1], match[2])
		// if player removed from server
		case reRemoved.MatchString(line):
			match := reRemoved.FindStringSubmatch(line)
			if s.Collector.Count > 0 {
				s.Collector.Count--
			}
			fmt.Printf("Removed: %s %s\n", match[1], match[2])
		}
	}
}

type Metrics struct {
	Count       int
	State       string
	Progress    float64
	StateString string
	states      map[string]prometheus.Metric
	descCounter *prometheus.Desc
	destState   *prometheus.Desc
}

func (m *Metrics) Describe(descs chan<- *prometheus.Desc) {
	descs <- m.descCounter
}

func NewCollector() *Metrics {
	descState := prometheus.NewDesc("state", "Current state of server", []string{"name"}, nil)
	return &Metrics{
		descCounter: prometheus.NewDesc("connections", "Number of connections", nil, nil),
		destState:   descState,
		StateString: " ",
		states: map[string]prometheus.Metric{
			"reconfiguring":    prometheus.MustNewConstMetric(descState, prometheus.GaugeValue, 0, "reconfiguring"),
			"preallocating":    prometheus.MustNewConstMetric(descState, prometheus.GaugeValue, 0, "preallocating"),
			"downloading":      prometheus.MustNewConstMetric(descState, prometheus.GaugeValue, 0, "downloading"),
			"verifying update": prometheus.MustNewConstMetric(descState, prometheus.GaugeValue, 0, "verifying update"),
			"committing":       prometheus.MustNewConstMetric(descState, prometheus.GaugeValue, 0, "committing"),
		},
	}
}

func (m *Metrics) Collect(ch chan<- prometheus.Metric) {
	if m.Count < 0 {
		ch <- prometheus.NewInvalidMetric(m.descCounter, fmt.Errorf("Count is negative"))
	} else {
		ch <- prometheus.MustNewConstMetric(m.descCounter, prometheus.GaugeValue, float64(m.Count))
	}
	if m.State != "" {
		if m.State != "installed" {
			m.states[m.State] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, m.Progress, m.State)
		} else {
			for _, el := range []string{"reconfiguring", "preallocating", "downloading", "verifying update", "staging", "committing"} {
				m.states[el] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, el)
			}
		}
		switch m.State {
		case "preallocating":
			m.states["reconfiguring"] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, "reconfiguring")
		case "downloading":
			m.states["preallocating"] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, "preallocating")
		case "verifying update":
			m.states["downloading"] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, "downloading")
		case "committing":
			m.states["verifying update"] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, "verifying update")
		case "installed":
			m.states["committing"] = prometheus.MustNewConstMetric(m.destState, prometheus.GaugeValue, 100, "committing")
		}
	}
	for _, metric := range m.states {
		ch <- metric
	}
}

var (
	logSteam = "/Users/egeneralov/go/src/gitlab.com/egeneralov/satisfactory-sidecar/steam.txt"
	logGame  = "/Users/egeneralov/go/src/gitlab.com/egeneralov/satisfactory-sidecar/log.txt"
	bind     = "0.0.0.0:2112"
	every    = 10 * time.Second
)

func init() {
	flag.StringVar(&logSteam, "steam", logSteam, "Path to steamcmd log")
	flag.StringVar(&logGame, "game", logGame, "Path to game log")
	flag.StringVar(&bind, "bind", bind, "Bind address")
	flag.DurationVar(&every, "every", every, "How often to print the current player count")
	flag.Parse()
}

func main() {
	sc := NewSideCar()
	go sc.Print(every)
	go func() {
		prometheus.MustRegister(sc.Collector)
		http.Handle("/metrics", promhttp.Handler())
		if err := http.ListenAndServe(bind, nil); err != nil {
			panic(err)
		}
	}()
	go sc.Tail(logSteam)
	sc.Tail(logGame)
}
